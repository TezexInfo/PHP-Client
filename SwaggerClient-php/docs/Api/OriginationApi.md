# Swagger\Client\OriginationApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getOrigination**](OriginationApi.md#getOrigination) | **GET** /origination/{origination_hash} | Get Origination


# **getOrigination**
> \Swagger\Client\Model\Origination getOrigination($origination_hash)

Get Origination

Get a specific Origination

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\OriginationApi();
$origination_hash = "origination_hash_example"; // string | The hash of the Origination to retrieve

try {
    $result = $api_instance->getOrigination($origination_hash);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OriginationApi->getOrigination: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **origination_hash** | **string**| The hash of the Origination to retrieve |

### Return type

[**\Swagger\Client\Model\Origination**](../Model/Origination.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

