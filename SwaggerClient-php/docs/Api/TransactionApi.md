# Swagger\Client\TransactionApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getTransaction**](TransactionApi.md#getTransaction) | **GET** /transaction/{transaction_hash} | Get Transaction
[**getTransactionsRecent**](TransactionApi.md#getTransactionsRecent) | **GET** /transactions/recent | Returns the last 50 Transactions
[**transactionsAll**](TransactionApi.md#transactionsAll) | **GET** /transactions/all | Get All Transactions
[**transactionsByLevelRange**](TransactionApi.md#transactionsByLevelRange) | **GET** /transactions/{startlevel}/{stoplevel} | Get All Transactions for a specific Level-Range


# **getTransaction**
> \Swagger\Client\Model\Transaction getTransaction($transaction_hash)

Get Transaction

Get a specific Transaction

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\TransactionApi();
$transaction_hash = "transaction_hash_example"; // string | The hash of the Transaction to retrieve

try {
    $result = $api_instance->getTransaction($transaction_hash);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransactionApi->getTransaction: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **transaction_hash** | **string**| The hash of the Transaction to retrieve |

### Return type

[**\Swagger\Client\Model\Transaction**](../Model/Transaction.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getTransactionsRecent**
> \Swagger\Client\Model\Transaction[] getTransactionsRecent()

Returns the last 50 Transactions

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\TransactionApi();

try {
    $result = $api_instance->getTransactionsRecent();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransactionApi->getTransactionsRecent: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\Transaction[]**](../Model/Transaction.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **transactionsAll**
> \Swagger\Client\Model\TransactionRange transactionsAll($page, $order, $limit)

Get All Transactions

Get all Transactions

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\TransactionApi();
$page = 3.4; // float | Pagination, 200 tx per page max
$order = "order_example"; // string | ASC or DESC
$limit = 56; // int | Results per Page

try {
    $result = $api_instance->transactionsAll($page, $order, $limit);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransactionApi->transactionsAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **float**| Pagination, 200 tx per page max | [optional]
 **order** | **string**| ASC or DESC | [optional]
 **limit** | **int**| Results per Page | [optional]

### Return type

[**\Swagger\Client\Model\TransactionRange**](../Model/TransactionRange.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **transactionsByLevelRange**
> \Swagger\Client\Model\TransactionRange transactionsByLevelRange($startlevel, $stoplevel, $page, $order, $limit)

Get All Transactions for a specific Level-Range

Get all Transactions for a specific Level-Range

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\TransactionApi();
$startlevel = 3.4; // float | lowest blocklevel to return
$stoplevel = 3.4; // float | highest blocklevel to return
$page = 3.4; // float | Pagination, 200 tx per page max
$order = "order_example"; // string | ASC or DESC
$limit = 56; // int | Results per Page

try {
    $result = $api_instance->transactionsByLevelRange($startlevel, $stoplevel, $page, $order, $limit);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TransactionApi->transactionsByLevelRange: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startlevel** | **float**| lowest blocklevel to return |
 **stoplevel** | **float**| highest blocklevel to return |
 **page** | **float**| Pagination, 200 tx per page max | [optional]
 **order** | **string**| ASC or DESC | [optional]
 **limit** | **int**| Results per Page | [optional]

### Return type

[**\Swagger\Client\Model\TransactionRange**](../Model/TransactionRange.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

