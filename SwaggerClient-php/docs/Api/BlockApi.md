# Swagger\Client\BlockApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**blocksAll**](BlockApi.md#blocksAll) | **GET** /blocks/all | Get All Blocks
[**blocksByLevel**](BlockApi.md#blocksByLevel) | **GET** /blocks/{level} | Get All Blocks for a specific Level
[**blocksByLevelRange**](BlockApi.md#blocksByLevelRange) | **GET** /blocks/{startlevel}/{stoplevel} | Get All Blocks for a specific Level-Range
[**getBlock**](BlockApi.md#getBlock) | **GET** /block/{blockhash} | Get Block By Blockhash
[**getBlockDelegations**](BlockApi.md#getBlockDelegations) | **GET** /block/{blockhash}/operations/delegations | Get Delegations of a Block
[**getBlockEndorsements**](BlockApi.md#getBlockEndorsements) | **GET** /block/{blockhash}/operations/endorsements | Get Endorsements of a Block
[**getBlockOperationsSorted**](BlockApi.md#getBlockOperationsSorted) | **GET** /block/{blockhash}/operations | Get operations of a block, sorted
[**getBlockOriginations**](BlockApi.md#getBlockOriginations) | **GET** /block/{blockhash}/operations/originations | Get Originations of a Block
[**getBlockTransaction**](BlockApi.md#getBlockTransaction) | **GET** /block/{blockhash}/operations/transactions | Get Transactions of Block
[**recentBlocks**](BlockApi.md#recentBlocks) | **GET** /blocks/recent | returns the last 25 blocks


# **blocksAll**
> \Swagger\Client\Model\BlocksAll blocksAll($page, $order, $limit)

Get All Blocks

Get all Blocks

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\BlockApi();
$page = 3.4; // float | Pagination, 200 tx per page max
$order = "order_example"; // string | ASC or DESC
$limit = 56; // int | Results per Page

try {
    $result = $api_instance->blocksAll($page, $order, $limit);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BlockApi->blocksAll: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **float**| Pagination, 200 tx per page max | [optional]
 **order** | **string**| ASC or DESC | [optional]
 **limit** | **int**| Results per Page | [optional]

### Return type

[**\Swagger\Client\Model\BlocksAll**](../Model/BlocksAll.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **blocksByLevel**
> \Swagger\Client\Model\Block[] blocksByLevel($level)

Get All Blocks for a specific Level

Get all Blocks for a specific Level

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\BlockApi();
$level = 3.4; // float | The level of the Blocks to retrieve, includes abandoned

try {
    $result = $api_instance->blocksByLevel($level);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BlockApi->blocksByLevel: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **level** | **float**| The level of the Blocks to retrieve, includes abandoned |

### Return type

[**\Swagger\Client\Model\Block[]**](../Model/Block.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **blocksByLevelRange**
> \Swagger\Client\Model\BlockRange blocksByLevelRange($startlevel, $stoplevel)

Get All Blocks for a specific Level-Range

Get all Blocks for a specific Level-Range

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\BlockApi();
$startlevel = 3.4; // float | lowest blocklevel to return
$stoplevel = 3.4; // float | highest blocklevel to return

try {
    $result = $api_instance->blocksByLevelRange($startlevel, $stoplevel);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BlockApi->blocksByLevelRange: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startlevel** | **float**| lowest blocklevel to return |
 **stoplevel** | **float**| highest blocklevel to return |

### Return type

[**\Swagger\Client\Model\BlockRange**](../Model/BlockRange.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getBlock**
> \Swagger\Client\Model\Block getBlock($blockhash)

Get Block By Blockhash

Get a block by its hash

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\BlockApi();
$blockhash = "blockhash_example"; // string | The hash of the Block to retrieve

try {
    $result = $api_instance->getBlock($blockhash);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BlockApi->getBlock: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **string**| The hash of the Block to retrieve |

### Return type

[**\Swagger\Client\Model\Block**](../Model/Block.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getBlockDelegations**
> \Swagger\Client\Model\Delegation[] getBlockDelegations($blockhash)

Get Delegations of a Block

Get all Delegations of a specific Block

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\BlockApi();
$blockhash = "blockhash_example"; // string | Blockhash

try {
    $result = $api_instance->getBlockDelegations($blockhash);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BlockApi->getBlockDelegations: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **string**| Blockhash |

### Return type

[**\Swagger\Client\Model\Delegation[]**](../Model/Delegation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getBlockEndorsements**
> \Swagger\Client\Model\Endorsement[] getBlockEndorsements($blockhash)

Get Endorsements of a Block

Get all Endorsements of a specific Block

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\BlockApi();
$blockhash = "blockhash_example"; // string | Blockhash

try {
    $result = $api_instance->getBlockEndorsements($blockhash);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BlockApi->getBlockEndorsements: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **string**| Blockhash |

### Return type

[**\Swagger\Client\Model\Endorsement[]**](../Model/Endorsement.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getBlockOperationsSorted**
> \Swagger\Client\Model\BlockOperationsSorted getBlockOperationsSorted($blockhash)

Get operations of a block, sorted

Get the maximum Level we have seen, Blocks at this level may become abandoned Blocks later on

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\BlockApi();
$blockhash = "blockhash_example"; // string | The hash of the Block to retrieve

try {
    $result = $api_instance->getBlockOperationsSorted($blockhash);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BlockApi->getBlockOperationsSorted: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **string**| The hash of the Block to retrieve |

### Return type

[**\Swagger\Client\Model\BlockOperationsSorted**](../Model/BlockOperationsSorted.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getBlockOriginations**
> \Swagger\Client\Model\Origination[] getBlockOriginations($blockhash)

Get Originations of a Block

Get all Originations of a spcific Block

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\BlockApi();
$blockhash = "blockhash_example"; // string | Blockhash

try {
    $result = $api_instance->getBlockOriginations($blockhash);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BlockApi->getBlockOriginations: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **string**| Blockhash |

### Return type

[**\Swagger\Client\Model\Origination[]**](../Model/Origination.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getBlockTransaction**
> \Swagger\Client\Model\Transaction[] getBlockTransaction($blockhash)

Get Transactions of Block

Get all Transactions of a spcific Block

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\BlockApi();
$blockhash = "blockhash_example"; // string | Blockhash

try {
    $result = $api_instance->getBlockTransaction($blockhash);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BlockApi->getBlockTransaction: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **string**| Blockhash |

### Return type

[**\Swagger\Client\Model\Transaction[]**](../Model/Transaction.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **recentBlocks**
> \Swagger\Client\Model\Block[] recentBlocks()

returns the last 25 blocks

Get all Blocks for a specific Level

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\BlockApi();

try {
    $result = $api_instance->recentBlocks();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BlockApi->recentBlocks: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\Block[]**](../Model/Block.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

