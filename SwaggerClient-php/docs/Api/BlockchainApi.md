# Swagger\Client\BlockchainApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**blockheight**](BlockchainApi.md#blockheight) | **GET** /maxLevel | Get Max Blockheight


# **blockheight**
> \Swagger\Client\Model\Level blockheight()

Get Max Blockheight

Get the maximum Level we have seen

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\BlockchainApi();

try {
    $result = $api_instance->blockheight();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BlockchainApi->blockheight: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\Level**](../Model/Level.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

