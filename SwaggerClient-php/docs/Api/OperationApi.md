# Swagger\Client\OperationApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getOperation**](OperationApi.md#getOperation) | **GET** /operation/{operation_hash} | Get Operation


# **getOperation**
> \Swagger\Client\Model\Operation getOperation($operation_hash)

Get Operation

Get a specific Operation

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\OperationApi();
$operation_hash = "operation_hash_example"; // string | The hash of the Operation to retrieve

try {
    $result = $api_instance->getOperation($operation_hash);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperationApi->getOperation: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **operation_hash** | **string**| The hash of the Operation to retrieve |

### Return type

[**\Swagger\Client\Model\Operation**](../Model/Operation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

