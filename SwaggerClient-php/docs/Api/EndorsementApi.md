# Swagger\Client\EndorsementApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getEndorsement**](EndorsementApi.md#getEndorsement) | **GET** /endorsement/{endorsement_hash} | Get Endorsement
[**getEndorsementForBlock**](EndorsementApi.md#getEndorsementForBlock) | **GET** /endorsement/for/{block_hash} | Get Endorsement


# **getEndorsement**
> \Swagger\Client\Model\Endorsement getEndorsement($endorsement_hash)

Get Endorsement

Get a specific Endorsement

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\EndorsementApi();
$endorsement_hash = "endorsement_hash_example"; // string | The hash of the Endorsement to retrieve

try {
    $result = $api_instance->getEndorsement($endorsement_hash);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EndorsementApi->getEndorsement: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **endorsement_hash** | **string**| The hash of the Endorsement to retrieve |

### Return type

[**\Swagger\Client\Model\Endorsement**](../Model/Endorsement.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getEndorsementForBlock**
> \Swagger\Client\Model\Endorsement[] getEndorsementForBlock($block_hash)

Get Endorsement

Get a specific Endorsement

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\EndorsementApi();
$block_hash = "block_hash_example"; // string | blockhash

try {
    $result = $api_instance->getEndorsementForBlock($block_hash);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EndorsementApi->getEndorsementForBlock: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **block_hash** | **string**| blockhash |

### Return type

[**\Swagger\Client\Model\Endorsement[]**](../Model/Endorsement.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

