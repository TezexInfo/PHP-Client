# Swagger\Client\NetworkApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**network**](NetworkApi.md#network) | **GET** /network | Get Network Information


# **network**
> \Swagger\Client\Model\NetworkInfo network()

Get Network Information

Get Network Information

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\NetworkApi();

try {
    $result = $api_instance->network();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NetworkApi->network: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\NetworkInfo**](../Model/NetworkInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

