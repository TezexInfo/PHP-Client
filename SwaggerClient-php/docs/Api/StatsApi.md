# Swagger\Client\StatsApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getStatistics**](StatsApi.md#getStatistics) | **GET** /stats/{group}/{stat}/{period} | Get Statistics
[**getStatsOverview**](StatsApi.md#getStatsOverview) | **GET** /stats/overview | Returns some basic Info


# **getStatistics**
> \Swagger\Client\Model\Stats[] getStatistics($group, $stat, $period, $start_time, $end_time)

Get Statistics

Get Statistics

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\StatsApi();
$group = "group_example"; // string | Block, Transaction, etc
$stat = "stat_example"; // string | 
$period = "period_example"; // string | 
$start_time = new \DateTime(); // \DateTime | 
$end_time = new \DateTime(); // \DateTime | 

try {
    $result = $api_instance->getStatistics($group, $stat, $period, $start_time, $end_time);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StatsApi->getStatistics: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group** | **string**| Block, Transaction, etc |
 **stat** | **string**|  |
 **period** | **string**|  |
 **start_time** | **\DateTime**|  | [optional]
 **end_time** | **\DateTime**|  | [optional]

### Return type

[**\Swagger\Client\Model\Stats[]**](../Model/Stats.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getStatsOverview**
> \Swagger\Client\Model\StatsOverview getStatsOverview()

Returns some basic Info

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\StatsApi();

try {
    $result = $api_instance->getStatsOverview();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StatsApi->getStatsOverview: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\StatsOverview**](../Model/StatsOverview.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

