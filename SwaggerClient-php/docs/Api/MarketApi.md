# Swagger\Client\MarketApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**candlestick**](MarketApi.md#candlestick) | **GET** /price/{denominator}/{numerator}/{period} | Candlestick Data
[**ticker**](MarketApi.md#ticker) | **GET** /ticker/{numerator} | Get Ticker for a specific Currency


# **candlestick**
> \Swagger\Client\Model\Candlestick[] candlestick($denominator, $numerator, $period)

Candlestick Data

Returns CandleStick Prices

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\MarketApi();
$denominator = "denominator_example"; // string | which currency
$numerator = "numerator_example"; // string | to which currency
$period = "period_example"; // string | Timeframe of one candle

try {
    $result = $api_instance->candlestick($denominator, $numerator, $period);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MarketApi->candlestick: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **denominator** | **string**| which currency |
 **numerator** | **string**| to which currency |
 **period** | **string**| Timeframe of one candle |

### Return type

[**\Swagger\Client\Model\Candlestick[]**](../Model/Candlestick.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **ticker**
> \Swagger\Client\Model\Ticker ticker($numerator)

Get Ticker for a specific Currency

Returns BTC, USD, EUR and CNY Prices

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\MarketApi();
$numerator = "numerator_example"; // string | The level of the Blocks to retrieve

try {
    $result = $api_instance->ticker($numerator);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MarketApi->ticker: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **numerator** | **string**| The level of the Blocks to retrieve |

### Return type

[**\Swagger\Client\Model\Ticker**](../Model/Ticker.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

