# Swagger\Client\DelegationApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getDelegation**](DelegationApi.md#getDelegation) | **GET** /delegation/{delegation_hash} | Get Delegation


# **getDelegation**
> \Swagger\Client\Model\Delegation getDelegation($delegation_hash)

Get Delegation

Get a specific Delegation

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\DelegationApi();
$delegation_hash = "delegation_hash_example"; // string | The hash of the Origination to retrieve

try {
    $result = $api_instance->getDelegation($delegation_hash);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DelegationApi->getDelegation: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delegation_hash** | **string**| The hash of the Origination to retrieve |

### Return type

[**\Swagger\Client\Model\Delegation**](../Model/Delegation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

