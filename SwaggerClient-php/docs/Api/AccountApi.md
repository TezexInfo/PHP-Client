# Swagger\Client\AccountApi

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAccount**](AccountApi.md#getAccount) | **GET** /account/{account} | Get Account
[**getAccountBalance**](AccountApi.md#getAccountBalance) | **GET** /account/{account}/balance | Get Account Balance
[**getAccountLastSeen**](AccountApi.md#getAccountLastSeen) | **GET** /account/{account}/last_seen | Get last active date
[**getAccountOperationCount**](AccountApi.md#getAccountOperationCount) | **GET** /account/{account}/operations_count | Get operation count of Account
[**getAccountTransactionCount**](AccountApi.md#getAccountTransactionCount) | **GET** /account/{account}/transaction_count | Get transaction count of Account
[**getDelegationsForAccount**](AccountApi.md#getDelegationsForAccount) | **GET** /account/{account}/delegations | Get Delegations of this account
[**getDelegationsToAccount**](AccountApi.md#getDelegationsToAccount) | **GET** /account/{account}/delegated | Get Delegations to this account
[**getEndorsementsForAccount**](AccountApi.md#getEndorsementsForAccount) | **GET** /account/{account}/endorsements | Get Endorsements this Account has made
[**getTransactionForAccountIncoming**](AccountApi.md#getTransactionForAccountIncoming) | **GET** /account/{account}/transactions/incoming | Get Transaction
[**getTransactionForAccountOutgoing**](AccountApi.md#getTransactionForAccountOutgoing) | **GET** /account/{account}/transactions/outgoing | Get Transaction


# **getAccount**
> \Swagger\Client\Model\Account getAccount($account)

Get Account

Get Acccount

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AccountApi();
$account = "account_example"; // string | The account

try {
    $result = $api_instance->getAccount($account);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountApi->getAccount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **string**| The account |

### Return type

[**\Swagger\Client\Model\Account**](../Model/Account.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAccountBalance**
> string getAccountBalance($account)

Get Account Balance

Get Balance

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AccountApi();
$account = "account_example"; // string | The account

try {
    $result = $api_instance->getAccountBalance($account);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountApi->getAccountBalance: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **string**| The account |

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAccountLastSeen**
> \DateTime getAccountLastSeen($account)

Get last active date

Get LastSeen Date

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AccountApi();
$account = "account_example"; // string | The account

try {
    $result = $api_instance->getAccountLastSeen($account);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountApi->getAccountLastSeen: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **string**| The account |

### Return type

[**\DateTime**](../Model/\DateTime.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAccountOperationCount**
> int getAccountOperationCount($account)

Get operation count of Account

Get Operation Count

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AccountApi();
$account = "account_example"; // string | The account

try {
    $result = $api_instance->getAccountOperationCount($account);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountApi->getAccountOperationCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **string**| The account |

### Return type

**int**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAccountTransactionCount**
> int getAccountTransactionCount($account)

Get transaction count of Account

Get Transaction Count

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AccountApi();
$account = "account_example"; // string | The account

try {
    $result = $api_instance->getAccountTransactionCount($account);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountApi->getAccountTransactionCount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **string**| The account |

### Return type

**int**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getDelegationsForAccount**
> \Swagger\Client\Model\Delegation[] getDelegationsForAccount($account, $before)

Get Delegations of this account

Get Delegations this Account has made

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AccountApi();
$account = "account_example"; // string | The account for which to retrieve Delegations
$before = 56; // int | Only Return Delegations before this blocklevel

try {
    $result = $api_instance->getDelegationsForAccount($account, $before);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountApi->getDelegationsForAccount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **string**| The account for which to retrieve Delegations |
 **before** | **int**| Only Return Delegations before this blocklevel | [optional]

### Return type

[**\Swagger\Client\Model\Delegation[]**](../Model/Delegation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getDelegationsToAccount**
> \Swagger\Client\Model\Delegation[] getDelegationsToAccount($account, $before)

Get Delegations to this account

Get that have been made to this Account

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AccountApi();
$account = "account_example"; // string | The account to which delegations have been made
$before = 56; // int | Only Return Delegations before this blocklevel

try {
    $result = $api_instance->getDelegationsToAccount($account, $before);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountApi->getDelegationsToAccount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **string**| The account to which delegations have been made |
 **before** | **int**| Only Return Delegations before this blocklevel | [optional]

### Return type

[**\Swagger\Client\Model\Delegation[]**](../Model/Delegation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getEndorsementsForAccount**
> \Swagger\Client\Model\Endorsement[] getEndorsementsForAccount($account, $before)

Get Endorsements this Account has made

Get Endorsements this Account has made

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AccountApi();
$account = "account_example"; // string | The account for which to retrieve Endorsements
$before = 56; // int | Only Return Delegations before this blocklevel

try {
    $result = $api_instance->getEndorsementsForAccount($account, $before);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountApi->getEndorsementsForAccount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **string**| The account for which to retrieve Endorsements |
 **before** | **int**| Only Return Delegations before this blocklevel | [optional]

### Return type

[**\Swagger\Client\Model\Endorsement[]**](../Model/Endorsement.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getTransactionForAccountIncoming**
> \Swagger\Client\Model\Transactions getTransactionForAccountIncoming($account, $before)

Get Transaction

Get incoming Transactions for a specific Account

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AccountApi();
$account = "account_example"; // string | The account for which to retrieve incoming Transactions
$before = 56; // int | Only Return transactions before this blocklevel

try {
    $result = $api_instance->getTransactionForAccountIncoming($account, $before);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountApi->getTransactionForAccountIncoming: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **string**| The account for which to retrieve incoming Transactions |
 **before** | **int**| Only Return transactions before this blocklevel | [optional]

### Return type

[**\Swagger\Client\Model\Transactions**](../Model/Transactions.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getTransactionForAccountOutgoing**
> \Swagger\Client\Model\Transactions getTransactionForAccountOutgoing($account, $before)

Get Transaction

Get outgoing Transactions for a specific Account

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$api_instance = new Swagger\Client\Api\AccountApi();
$account = "account_example"; // string | The account for which to retrieve outgoing Transactions
$before = 56; // int | Only return transactions before this blocklevel

try {
    $result = $api_instance->getTransactionForAccountOutgoing($account, $before);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountApi->getTransactionForAccountOutgoing: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **string**| The account for which to retrieve outgoing Transactions |
 **before** | **int**| Only return transactions before this blocklevel | [optional]

### Return type

[**\Swagger\Client\Model\Transactions**](../Model/Transactions.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

