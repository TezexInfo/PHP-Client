# Ticker

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**btc** | **string** |  | [optional] 
**cny** | **string** |  | [optional] 
**usd** | **string** |  | [optional] 
**eur** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


