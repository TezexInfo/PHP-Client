# BlocksAll

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**max_block_level** | **int** |  | [optional] 
**blocks** | [**\Swagger\Client\Model\Block[]**](Block.md) |  | [optional] 
**total_results** | **int** |  | [optional] 
**current_page** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


