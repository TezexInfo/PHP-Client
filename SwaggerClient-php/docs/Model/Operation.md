# Operation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transaction** | [**\Swagger\Client\Model\Transaction**](Transaction.md) |  | [optional] 
**origination** | [**\Swagger\Client\Model\Origination**](Origination.md) |  | [optional] 
**delegation** | [**\Swagger\Client\Model\Delegation**](Delegation.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


