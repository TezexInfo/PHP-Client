# OriginationOperation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**kind** | **string** |  | [optional] 
**manager_pubkey** | **string** |  | [optional] 
**balance** | **int** |  | [optional] 
**spendable** | **bool** |  | [optional] 
**delegateable** | **bool** |  | [optional] 
**delegate** | **string** |  | [optional] 
**script** | [**\Swagger\Client\Model\TezosScript**](TezosScript.md) |  | [optional] 
**storage** | [**\Swagger\Client\Model\TezosScript**](TezosScript.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


