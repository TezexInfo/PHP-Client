# Candlestick

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**o** | **string** |  | [optional] 
**h** | **string** |  | [optional] 
**l** | **string** |  | [optional] 
**c** | **string** |  | [optional] 
**vol** | **string** |  | [optional] 
**time** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


