# BlockOperationsSorted

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transactions** | [**\Swagger\Client\Model\Transaction[]**](Transaction.md) |  | [optional] 
**originations** | [**\Swagger\Client\Model\Origination[]**](Origination.md) |  | [optional] 
**delegations** | [**\Swagger\Client\Model\Delegation[]**](Delegation.md) |  | [optional] 
**endorsements** | [**\Swagger\Client\Model\Endorsement[]**](Endorsement.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


