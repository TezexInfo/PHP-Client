# Transaction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hash** | **string** |  | [optional] 
**branch** | **string** |  | [optional] 
**source** | **string** |  | [optional] 
**public_key** | **string** |  | [optional] 
**level** | **int** |  | [optional] 
**block_hash** | **string** |  | [optional] 
**counter** | **int** |  | [optional] 
**time** | [**\DateTime**](\DateTime.md) |  | [optional] 
**operations** | [**\Swagger\Client\Model\TransactionOperation[]**](TransactionOperation.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


