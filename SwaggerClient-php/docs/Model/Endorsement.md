# Endorsement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hash** | **string** |  | [optional] 
**source** | **string** |  | [optional] 
**level** | **int** |  | [optional] 
**block** | **string** |  | [optional] 
**endorsed_block** | **string** |  | [optional] 
**slot** | **int** |  | [optional] 
**time** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


