# TezosScript

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**int** | **string** |  | [optional] 
**string** | **string** |  | [optional] 
**prim** | **string** |  | [optional] 
**args** | [**\Swagger\Client\Model\TezosScript[]**](TezosScript.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


